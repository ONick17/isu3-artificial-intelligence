import pickle
import numpy as np
from sklearn.metrics import confusion_matrix

fl = np.loadtxt("b_test.dat")
X_test = fl[:, :-1]
Y_test = fl[:, -1]

fl = open("test2.pkl", 'rb')
k_model = pickle.load(fl)
fl.close()

k_ans = k_model.predict(X_test)
print(confusion_matrix(Y_test, k_ans))
