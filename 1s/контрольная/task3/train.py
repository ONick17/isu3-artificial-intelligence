import numpy as np
from sklearn import tree
from sklearn.metrics import accuracy_score
import pickle

fl = np.loadtxt("src.dat")
X = fl[:, :-1].astype(float)
Y = fl[:, -1].astype(int)

model = tree.DecisionTreeClassifier()
model.fit(X, Y)
ans = model.predict(X)
print(accuracy_score(Y, ans))

fl = open("test3.pkl", 'wb')
pickle.dump(model, fl)
fl.close()
