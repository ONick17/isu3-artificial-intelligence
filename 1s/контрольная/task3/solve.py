import pickle
import numpy as np
from sklearn.metrics import accuracy_score

fl = np.loadtxt("src.dat")
X = fl[:, :-1].astype(float)
Y = fl[:, -1].astype(int)

fl = open("test3.pkl", 'rb')
model2 = pickle.load(fl)
fl.close()
ans = model2.predict(X)
print(accuracy_score(Y, ans))
