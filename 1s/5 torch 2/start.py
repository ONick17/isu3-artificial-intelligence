import torch
import numpy as np

class FirstNet(torch.nn.Module):
    def __init__(self, n_hidden_neurons):
        super(FirstNet, self).__init__()
        self.fc1 = torch.nn.Linear(1, n_hidden_neurons)
        self.sm = torch.nn.Softmax(dim=1)

    def forward(self, x):
        x = self.fc1(x)
        return x

    def inference(self, x):
        x = self.forward(x)
        x = self.sm(x)
        return x

import sys
f=sys.stdin
l=f.read().split("\n")
xn=[]
yn=[]
for s in l:
 z=s.split()
 if(z):
  xn.append([float(z[0])])
  yn.append(int(z[1]))

x=torch.tensor(xn)
y=torch.tensor(yn)

first_net = FirstNet(3)

loss = torch.nn.CrossEntropyLoss()

optimizer = torch.optim.Adam(first_net.parameters(),lr=1.0e-3)

for epoch in range(5000):
 optimizer.zero_grad()
 x_batch = x
 y_batch = y

 preds = first_net.forward(x_batch)
 loss_value = loss(preds, y_batch)
 loss_value.backward()       
 optimizer.step()
        
 if epoch % 100 == 0:
  test_preds = first_net.forward(x)
  test_preds = test_preds.argmax(dim=1)
  print((test_preds == y).float().mean())


# test_preds = first_net.forward(x)
# test_preds = test_preds.argmax(dim=1)
# for i in range(y.size()[0]):
#  print(x[i],y[i],test_preds[i])
