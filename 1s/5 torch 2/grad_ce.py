import torch
import numpy as np

def softmax(x,w,n):
 return 1.0/(torch.exp(w[0][0]*x+w[0][1]-(w[n][0]*x+w[n][1]))+torch.exp(w[1][0]*x+w[1][1]-(w[n][0]*x+w[n][1]))+torch.exp(w[2][0]*x+w[2][1]-(w[n][0]*x+w[n][1])))

def ce_1(y_teacher,x,w):
 z=softmax(x,w,int(y_teacher))
 return -1.0*torch.log(z)

def ce(y_teacher,x,w):
 s=0.
 n=0.
 sze=y_teacher[0].size()[0]
 for i in range(sze):
  s+=ce_1(y_teacher[0][i],x[0][i],w)
  n+=1.
 return s/n

w=torch.tensor([[0.0,0.0],[0.0,0.0],[0.0,0.0]],requires_grad=True)
import sys
f=sys.stdin
l=f.read().split("\n")
xn=[]
yn=[]
for s in l:
 z=s.split()
 if(z):
  xn.append(float(z[0]))
  yn.append(float(z[1]))
print('fsdffsf')
x=torch.tensor([xn])
y=torch.tensor([yn])
for step in range(10001):
 alpha=0.00002
 function = ce(y,x,w)
 function.backward()
 w.data-=alpha*w.grad
 w.grad.zero_()
# if(step % 10 ==9):
 print (w.data[0],w.data[1],w.data[2],step)


