import torch
import numpy as np

def act(x,w):
 return 1./(1.+torch.exp(-(w[0]*x+w[1])))

def bce(y_teacher,y):
 return (-y_teacher*torch.log(y)-(1.-y_teacher)*torch.log(1.-y)).mean()


w=torch.tensor([[0.0],[0.0]],requires_grad=True)
import sys
f=sys.stdin
l=f.read().split("\n")
xn=[]
yn=[]
for s in l:
 z=s.split()
 if(z):
  xn.append(float(z[0]))
  yn.append(float(z[1]))

x=torch.tensor([xn])
y=torch.tensor([yn])
for step in range(200001):
# alpha=0.00003
 alpha=0.00005
 function = bce(y,act(x,w)) 
 function.backward()
 w.data-=alpha*w.grad
 w.grad.zero_()
 if(step % 100 ==99):
  print (w.data[0],w.data[1],step)


