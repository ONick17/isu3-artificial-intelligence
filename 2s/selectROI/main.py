import cv2

tracker = cv2.TrackerMIL_create()

cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)
#cv2.namedWindow("Camera", cv2.WINDOW_AUTOSIZE)
cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1)
cam.set(cv2.CAP_PROP_EXPOSURE, -4)

_, frame = cam.read()
roi = cv2.selectROI(frame, False)
tracker.init(frame, roi)

while True:
    _, frame = cam.read()
    success, roi = tracker.update(frame)
    (x, y, w, h) = tuple(map(int, roi))
    if success:
        cv2.rectangle(frame, (x,y), (x+w, y+h), (0, 255, 0), 3)
    cv2.imshow("Camera", frame)
    key = cv2.waitKey(1)
    if key == ord('q'):
        break
        

cam.release()
cv2.destroyAllWindows()
