import sklearn as skl

fl = open("responses.txt", 'r')
anss = fl.readlines()
fl.close()

matrix = {"TP": 0, "FP": 0, "FN": 0, "TN": 0}
for i in anss:
    match i:
        case "positive positive\n":
            matrix["TP"] += 1
        case "negative positive\n":
            matrix["FP"] += 1
        case "negative negative\n":
            matrix["FN"] += 1
        case "positive negative\n":
            matrix["TN"] += 1
print(f"TP: {matrix['TP']},\tFP: {matrix['FP']}")
print(f"FN: {matrix['FN']},\tTN: {matrix['TN']}")
print("Recall:", matrix['TP']/(matrix['TP']+matrix['FN']))
print("Precision:", matrix['TP']/(matrix['TP']+matrix['FP']))
print("Accuracy:", (matrix['TP']+matrix['TN'])/(matrix['TP']+matrix['FN']+matrix['TN']+matrix['FP']))
print("F1:", (2*matrix['TP'])/(2*matrix['TP']+matrix['FP']+matrix['FN']))
