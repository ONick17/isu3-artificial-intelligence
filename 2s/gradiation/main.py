import matplotlib.pyplot as plt
import numpy as np

#def f(x):
#    return x ** 2

#def f_grad(x):
#    return 2 * x

c = np.array(0.15 * np.pi)

def f(x):
    return x * np.cos(c*x)

def f_grad(x):
    return np.cos(c*x) - x*c*np.sin(c*x)

def gd(eta, f_grad, n=10):
    x = 10
    results = [x]
    for _ in range(n):
        x -= eta * f_grad(x)
        results.append(float(x))
    print(f"epoch 10, x: {x:.3f}")
    return np.array(results)

results = gd(0.1, f_grad)

lim = np.max([np.abs(results.min()), np.abs(results.max())])
f_x = np.arange(-lim, lim, 0.1)
plt.plot(f_x, f(f_x))

for i in range(1, len(results)):
    res = results[i-1:i+1]
    plt.plot(res, f(np.array(res)), 'o--')

plt.show()
