import cv2
import numpy as np
import matplotlib.pyplot as plt

conf = cv2.imread("conference.jpg")
cooper = cv2.imread("cooper.jpg")
face = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
lbp_face = cv2.CascadeClassifier("lbpcascade_frontalface.xml")

def detector(img, classifier, scale=None, min_nbs=None):
    result = img.copy()
    rects = classifier.detectMultiScale(result, scaleFactor=scale,
                                        minNeighbors=min_nbs)
    for (x, y, w, h)in rects:
        cv2.rectangle(result, (x, y), (x+w, y+h), (255, 255, 255))
    return result

conf_result = detector(conf, face, 1.2, 5)
cooper_result = detector(cooper, lbp_face, 1.2, 5)

plt.subplot(121)
plt.imshow(conf_result)
plt.subplot(122)
plt.imshow(cooper_result)

plt.show()
