import cv2
import matplotlib.pyplot as plt
import numpy as np

pennies = cv2.imread("pennies.jpg")
blurred = cv2.medianBlur(pennies, 25)
gray = cv2.cvtColor(blurred, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(gray, 160, 255, cv2.THRESH_BINARY_INV)

dist = cv2.distanceTransform(thresh, cv2.DIST_L2, 5)
fg = cv2.threshold(dist, 0.7*dist.max(), 255, 0)[1]
fg = np.uint8(fg)
confuse = cv2.subtract(thresh, fg)

markers_or = cv2.connectedComponents(fg)[1]
markers = markers_or + 1
markers[confuse==255] = 0

wmarkers = cv2.watershed(pennies, markers.copy())

contours, hierarchy = cv2.findContours(wmarkers, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)

pennies2 = pennies.copy()
for i in range(len(contours)):
    if hierarchy[0][i][3] == -1:
        cv2.drawContours(pennies2, contours, i, (0, i*30, 0), 10)

plt.subplot(161)
plt.imshow(pennies)
plt.subplot(162)
plt.imshow(fg)
plt.subplot(163)
plt.imshow(markers_or)
plt.subplot(164)
plt.imshow(markers)
plt.subplot(165)
plt.imshow(wmarkers)
plt.subplot(166)
plt.imshow(pennies2)

plt.show()
