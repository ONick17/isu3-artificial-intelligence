import cv2
import numpy as np


cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)
face = cv2.imread("face.jpg" , 0)
orb = cv2.SIFT_create()
key_points1, description1 = orb.detectAndCompute(face, None)
matcher = cv2.BFMatcher()

cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1)
cam.set(cv2.CAP_PROP_EXPOSURE, -4)
while cam.isOpened():
    ret, frame = cam.read()
    
    key_points2, description2 = orb.detectAndCompute(frame, None)
    matches =  matcher.knnMatch(description1, description2, k=2)
    best = []
    for m1, m2 in matches:
        if m1.distance < 0.75* m2.distance:
            best.append([m1])

    if len(best)>5:
        print(f"Matches: {len(best)}")
        src_pts = np.float32([key_points1[m[0].queryIdx].pt for m in best]).reshape(-1,1,2)
        dst_pts = np.float32([key_points2[m[0].trainIdx].pt for m in best]).reshape(-1,1,2)
        M, hmask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
        #print(M)
        if M is None:
            #print("LOLOLOLOLOLOLOLOLOL")
            pass
        else:
            w, h = face.shape
            pts = np.float32([[0,0], [0, h-1], [w-1, h-1], [w-1, 0]]).reshape(-1,1,2)
            dst = cv2.perspectiveTransform(pts, M)
            result = cv2.polylines(frame, [np.int32(dst)], True, 255, 1, cv2.LINE_AA)
    else:
        print("Not enough matches")
        mask = None
        
    matches_image = cv2.drawMatchesKnn(face, key_points1, frame, key_points2, best, None)    
    cv2.imshow("Camera", frame)
    key = cv2.waitKey(1)
    if key>0:
        if chr(key) == 'q':
            break

cam.release()
cv2.destroyAllWindows()
