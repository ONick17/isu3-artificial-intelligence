import cv2
import matplotlib.pyplot as plt
import numpy as np


np.random.seed(12)
n = 2000

xk1 = 100 + np.random.randint(-25, 25, n)
yk1 = 100 + np.random.randint(-25, 25, n)
rk1 = np.repeat(1, n)

xk2 = 150 + np.random.randint(-25, 25, n)
yk2 = 150 + np.random.randint(-25, 25, n)
rk2 = np.repeat(2, n)

new_point = (127, 124)

knn = cv2.ml.KNearest_create()
train = np.stack([np.hstack([xk1, xk2]), np.hstack([yk1, yk2])])
#print(train.shape)
train = train.T.astype("f4")

responses = np.hstack([rk1, rk2]).reshape(-1, 1).astype("f4")

knn.train(train, cv2.ml.ROW_SAMPLE, responses)

in_ = np.array(new_point).astype("f4").reshape(1, 2)
ret, results, neighbours, dist = knn.findNearest(in_, 2)

print(ret)
print(results)
print(neighbours)
print(dist)
plt.scatter(xk1, yk1, c="blue")
plt.scatter(xk2, yk2, c="red")
plt.scatter(new_point[0], new_point[1], c="green")
plt.show()
