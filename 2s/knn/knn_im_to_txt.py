import cv2
import matplotlib.pyplot as plt
import numpy as np
import pathlib
from tqdm import tqdm
from skimage.measure import label, regionprops


train_images = {}

for path in tqdm(sorted(pathlib.Path("out/train").glob("*"))):
    symbol = path.name[-1]
    train_images[symbol] = []
    for image_path in sorted(path.glob("*.png")):
        symbol = path.name[-1]
        train_images[symbol].append(plt.imread(image_path))
print()


def extract_features(image):
    if image.ndim == 3:
        gray = np.mean(image, 2)
        gray[gray > 0] = 1
        labeled = label(gray)
    else:
        labeled = image.astype("uint8")
    props = regionprops(labeled)[0]
    extent = props.extent
    eccentricity = props.eccentricity
    euler = props.euler_number
    rr, cc = props.centroid_local
    rr = rr / props.image.shape[0]
    cc = cc / props.image.shape[1]
    feret = (props.feret_diameter_max - 1) / (np.max(props.image.shape))
    #print(len(props))
    #return labeled
    return np.array([extent, eccentricity, euler, rr, cc, feret], dtype="f4")


'''
a1 = extract_features(train_images["A"][0])
a2 = extract_features(train_images["A"][1])
for i in range(len(a1)):
    print(a1[i])
    print(a2[i])
    print()
'''

knn = cv2.ml.KNearest_create()

train = []
responses = []

sym2class = {symbol: i for i, symbol in enumerate(train_images)}
class2sym = {value: key for key, value in sym2class.items()}

for i, symbol in tqdm(enumerate(train_images)):
    for image in train_images[symbol]:
        train.append(extract_features(image))
        responses.append(sym2class[symbol])

train = np.array(train, dtype="f4")
responses = np.array(responses).reshape(-1, 1)

knn.train(train, cv2.ml.ROW_SAMPLE, responses)

'''
features = extract_features(train_images["A"][0]).reshape(1, -1)
ret, results, neighbours, dist = knn.findNearest(features, 5)
print(ret, results, neighbours, dist)
print(class2sym[int(ret)])
'''

def image2text(image) -> str:
    gray = np.mean(image, 2)
    gray[gray > 0] = 1
    labeled = label(gray)
    regions = regionprops(labeled)
    answer = []
    regions_dic = {}
    for region in regions:
        regions_dic[region.bbox[1]] = region
    keke = 0
    delmin = 2**30
    delmax = 0
    for x in sorted(regions_dic):
        region = regions_dic[x]
        if region.bbox[1]-keke > delmax:
            delmax = region.bbox[1] - keke
        if (region.bbox[1]-keke < delmin) and (region.bbox[1]-keke > 0):
            delmin = region.bbox[1] - keke
        keke = region.bbox[3]
    space_thresh = (delmax+delmin)/2
    keke = 0
    for x in sorted(regions_dic):
        region = regions_dic[x]
        if region.bbox[1] - keke < 0:
            answer[-1] = 'i'
            keke = region.bbox[3]
            continue
        if region.bbox[1]-keke > space_thresh:
            answer.append(' ')
        features = extract_features(region.image).reshape(1, -1)
        ret, results, neighbours, dist = knn.findNearest(features, 5)
        answer.append(class2sym[int(ret)])
        keke = region.bbox[3]
    return "".join(answer)


print()
text_images = [plt.imread(path) for path in pathlib.Path("out").glob("*.png")]
#print(len(text_images))
for image in text_images:
    print(image2text(image))
