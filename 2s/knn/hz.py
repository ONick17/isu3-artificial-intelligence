import numpy as np
import cv2
import matplotlib.pyplot as plt

corners = {"maxCorners": 10,
           "qualityLevel": 0.3,
           "minDistance": 7,
           "blockSize": 7}

lk = {"winSize": (200, 200),
      "maxLevel": 2,
      "criteria": (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT,
                   10, 0.03)}

cv2.namedWindow("hz", cv2.WINDOW_AUTOSIZE)
cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_EXPOSURE, -5)

_, prev_frame = cam.read()

prev_gray = cv2.cvtColor(prev_frame, cv2.COLOR_BGR2GRAY)
prev_pts = cv2.goodFeaturesToTrack(prev_gray, mask=None, **corners)

mask = np.zeros_like(prev_frame)
while True:
    ret, frame = cam.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    next_pts, status, err = cv2.calcOpticalFlowPyrLK(prev_gray, gray,
                                                     prev_pts, None,
                                                     **lk)
    k = cv2.waitKey(30) & 0xFF
    good_new = next_pts[status == 1]
    good_prev = prev_pts[status == 1]
    
    for i, (new, prev) in enumerate(zip(good_new, good_prev)):
        x_new, y_new = new.ravel().astype("int")
        x_prev, y_prev = prev.ravel().astype("int")
        
        mask = cv2.line(mask, (x_new, y_new), (x_prev, y_prev),
                        (0, 255, 0), 3)
        frame = cv2.circle
    cv2.imshow("hz", frame)
    if k == 27:
        break
    
print(prev_pts)

cam.release()

plt.imshow(prev_gray)
x = [prev_pts[i, 0, 0] for i in range(prev_gray.shape[0])]
y = [prev_pts[i, 0, 1] for i in range(prev_gray.shape[0])]
plt.scatter(x, y)
plt.show()
