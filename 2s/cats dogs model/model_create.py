from tensorflow.keras.layers import Activation, Dropout, Flatten, Conv2D, MaxPooling2D, Dense
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import numpy as np
import matplotlib.pyplot as plt
import cv2


gen = ImageDataGenerator(rotation_range=30, width_shift_range=0.1, height_shift_range=0.1,
                         rescale=1/255, shear_range=0.2, zoom_range=0.2, horizontal_flip=True,
                         fill_mode="nearest")

input_shape = (150, 150, 3)
model = Sequential()
model.add(Conv2D(filters=32, kernel_size=(3,3), input_shape=input_shape, activation="relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Conv2D(filters=64, kernel_size=(3,3), activation="relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Conv2D(filters=64, kernel_size=(3,3), activation="relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
model.add(Dense(256))
model.add(Activation("relu"))
model.add(Dropout(0.2))
model.add(Dense(1))
model.add(Activation("sigmoid"))

model.summary()

model.compile(loss="binary_crossentropy", optimizer="adam", metrics=["accuracy"])

batch_size = 16

train_gen = gen.flow_from_directory("../CATS_DOGS/train", target_size=input_shape[:2],
                                    batch_size=batch_size, class_mode="binary")
test_gen = gen.flow_from_directory("../CATS_DOGS/test", target_size=input_shape[:2],
                                    batch_size=batch_size, class_mode="binary")

#print(test_gen.class_indices)
#test_gen.next()

#batch, classes = test_gen.next()
#plt.imshow(batch[0])
#plt.show()

model_trained = model.fit(train_gen, epochs=20, steps_per_epoch=150,
                          validation_data=test_gen, validation_steps=12)

model_trained.save("cats_dogs_model.h5")
