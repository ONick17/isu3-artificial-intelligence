def iou(bbox1: list, bbox2: list) -> float:
    #y ys x xs
    
    a = min(bbox1[2]+bbox1[3], bbox2[2]+bbox2[3]) - max(bbox1[2], bbox2[2])
    b = min(bbox1[0]+bbox1[1], bbox2[0]+bbox2[1]) - max(bbox1[0], bbox2[0])
    print(a)
    print(b)
    
    if (a < 0) or (b < 0):
        inter = 0
    else:
        inter = a*b
    print(inter)

    #union = bbox1[1]*bbox1[3] + bbox2[1]*bbox2[3] - inter
    union = bbox1[1]*bbox1[3]
    print(union)
    
    try:
        res = float(inter/union)
    except Exception:
        res = 0.0
    print(res)
    print()
    return res


bbox1 = [0, 10, 0, 10]
bbox2 = [0, 10, 1, 10]
bbox3 = [20, 30, 20, 30]
bbox4 = [5, 15, 5, 15]
assert iou(bbox1, bbox1) == 1.0
assert iou(bbox1, bbox2) == 0.9
assert iou(bbox1, bbox3) == 0.0
assert round(iou(bbox1, bbox4), 2) == 0.14
